# M2-BioInfo-BS2

## Binome:
	* Jules Garreau : @jules_g
	* Mario Papail : @PandoraMagis

# Getting-started

In a linux shell go to the folder where you want to clone the repository and execute :
```
git clone git@gitlab.com:PandoraMagis/m2-bioinfo-bs2.git
```

# Tests

## Launch tests

In order to launch  the tests for every chapters got to the roots folder of the project and then run the folowing :
```
make tests
```

## Add tests

In order to add test for a specific file :

Firsty, create a file in the project_root/tests/ folder and naming it starting with 'tests'

then respect the folowing template (exemple in order to test a module call 'bs2_chapN')

```python
import unittest
from utils import set_import_root, cd_to_test_folder


class TestTestCase(unittest.TestCase):
    """TestCase to test if the test works"""

    def test_test(self) -> None:
        """test if the tests works"""
        self.assertTrue(True)

#-----
#
#       put yout other unitest testcase classes here
#       calling class and function like : cN.my_function_from_module
#
#------

if __name__ == "__main__":
    print("----\n\nLaunch test for chapN\n\n----")
    set_import_root()  # set root in order to import functions/classes
    import bs2_chapN as cN  # import function and classes to tests

    cd_to_test_folder()  # cd to root in order to get test file
    unittest.main()  # perform tests
```
