from bs2_chap1 import (
    read_interaction_file_dict,
    read_interaction_file_list,
    is_interaction_file,
)
from pathlib import Path
from statistics import mean


def count_vertices(filename: Path) -> int:
    """retun the number of summit of a graph

    Args:
        filename (Path): tabulate interaction file path

    Returns:
        int: summit number
    """
    return len(read_interaction_file_dict(filename))


def clean_interactome(filein: Path, fileout: Path) -> None:
    """clean a graph file by remove doubles and homodimer

    Args:
        filein (Path): initial file
        fileout (Path): cleaned file
    """
    file_check, error_msg = is_interaction_file(filein)
    if file_check:
        found: list = []
        graph = ""
        for inter in read_interaction_file_list(filein):
            if inter in found or [inter[1], inter[0]] in found:
                pass
            elif inter[0] == inter[1]:
                pass
            else:
                graph += " ".join(inter) + "\n"
                found.append(inter)

        graph = str(len(found)) + "\n" + graph

        with open(fileout, "w") as out:
            out.write(graph)
    else:
        raise error_msg


def get_degree(file: Path, prot: str) -> int:
    """return degree of the given prot

    Args:
        file (Path): tabulate interaction file path
        prot (str): the given prot

    Returns:
        int: the degree of the prot
    """
    graph: dict = read_interaction_file_dict(file)
    if not prot in graph:
        raise ValueError("prot not in the graph")
    else:
        return len(graph[prot])


def get_max_degree(file: Path) -> list:
    """get the prots with max degree in the graph

    Args:
        file (Path): tabulate interaction file path

    Returns:
        (list): prot.s with the max degree.s
    """
    graph: dict = read_interaction_file_dict(file)
    degrees: dict = {}
    for prot in graph:
        if not len(graph[prot]) in degrees:
            degrees[len(graph[prot])] = [prot]
        else:
            degrees[len(graph[prot])].append(prot)

    return degrees[max([key for key in degrees.keys()])]


def get_ave_degree(file: Path) -> float:
    """get the average degree of the prots of the graph

    Args:
        file (Path): tabulate interaction file path

    Returns:
        float: average degree of the graph
    """
    graph: dict = read_interaction_file_dict(file)
    return mean([len(graph[prot]) for prot in graph])


def count_degree(file: Path, deg: int) -> int:
    """return number of protein with the given degree in the graph

    Args:
        file (Path): tabulate interaction file path
        deg (int): degree

    Returns:
        int: number of prot with the given degree
    """
    graph: dict = read_interaction_file_dict(file)
    return len([1 for prot in graph if len(graph[prot]) == deg])


def histogram_degree(file: Path, dmin: int, dmax: int) -> None:
    """print the number of prot with degree between dmin and dmax

    Args:
        file (Path): tabulate interaction file path
        dmin (int): minimum degree
        dmax (int): maximum degree
    """
    for degree in range(dmin, dmax + 1):
        print(str(degree) + " : " + "*" * count_degree(file, degree))


if __name__ == "__main__":
    pass
