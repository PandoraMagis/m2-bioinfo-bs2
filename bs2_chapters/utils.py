import os
from pathlib import Path


def cd_to_parent_folder() -> None:
    path_root = Path(__file__).parents[1]
    os.chdir(path_root)
