import random

from copy import deepcopy
from pathlib import Path
from statistics import mean
from typing import Tuple

from bs2_chap1 import *
from bs2_chap2 import clean_interactome


class Interactome:

    # -- constructor and builder --

    def __init__(self, filename: Path = Path(""), int_dict_input: dict = {}) -> None:
        """Init the interatom in different format base on a file OR a dict

        Args:
            filename (Path, optional): path to tabulate graph file. Defaults to "".
            int_dict_input (dict, optional): interaction dict. Defaults to {}.
        """
        self.__proteins: list = []
        self.__int_dict: dict = {}
        self.__int_list: list = []
        self.__int_mat: list = None  # type: ignore
        self.__filename: Path = Path("")

        if not filename == Path(""):
            self._setup_with_file_input(filename)
        elif int_dict_input:

            self._setup_with_dict_input(int_dict_input)

        elif filename and int_dict_input:
            raise ValueError(
                "Error two input type specified, must only be dict OR file"
            )
        else:
            raise ValueError("Error no input specified")

    def add_edge_to_dict(self, int_dict, protein1, protein2) -> dict:
        """Add an edge to the graph

        Args:
            int_dict (dict): dict of the edges of the graph
            protein1 (str): the first protein
            protein2 (str): the second protein
        """

        for x, y in [
            (protein1, protein2),
            (protein2, protein1),
        ]:  # do the if,elif,else for x AND y
            if x not in int_dict:
                int_dict[x] = y
            elif isinstance(int_dict[x], str):  # 1 edge = str, multiple = list
                int_dict[x] = [int_dict[x], y]
            else:
                int_dict[x].append(y)
        return int_dict

    def _setup_with_dict_input(self, int_dict_input: dict) -> None:
        """setup the interactome based on a dictionary interactome

        Args:
            int_dict_input (dict): input dictionary
        """
        self.int_dict = int_dict_input
        self.__build_format_from_dict()
        self.__build_proteins_list()

    def _setup_with_file_input(self, filename: Path) -> None:
        file_checked, error_msg = is_interaction_file(filename)  # verify file format
        if file_checked:
            clean_interactome(
                filein=filename, fileout=filename
            )  # clean doublon and homodimere
            self.__filename = filename
            self.__build_format_from_file()
            self.__build_proteins_list()
        else:
            raise error_msg

    def __build_format_from_dict(self) -> None:
        """Build the different interactom format for the given graph"""

        int_list: list = []
        for vert in self.int_dict:
            for vert_linked in self.int_dict[vert]:
                if (not [vert, vert_linked] in int_list) and (
                    not [vert_linked, vert] in int_list
                ):
                    int_list.append([vert, vert_linked])

        int_m: list = [
            [1 if y in self.int_dict[x] else 0 for y in sorted(self.int_dict)]
            for x in sorted(self.int_dict)
        ]
        # using sorted() on dict to make sure summit are sorted for matrix building

        # init Interactom instance attributes
        self.int_list: list = int_list
        self.int_mat: list = int_m

    def __build_format_from_file(self) -> None:
        """Build the different interactom format for the given graph"""

        int_reader: Generator = read_interaction_file_generator(self.__filename)
        int_dict: dict = {}
        int_list: list = []

        # With x, y as the two entity of the relation
        for X, Y in [line.split() for line in int_reader if len(line) >= 3]:
            if (not [X, Y] in int_list) and (not [Y, X] in int_list):
                int_list.append([X, Y])  # add relation to the int_list

            self.add_edge_to_dict(int_dict, X, Y)  # add relation to the int_dict

        int_m: list = [
            [1 if y in int_dict[x] else 0 for y in sorted(int_dict)]
            for x in sorted(int_dict)
        ]
        # using sorted() on dict to make sure summit are sorted for matrix building

        # init Interactom instance attributes
        self.int_list: list = int_list
        self.int_dict: dict = int_dict
        self.int_mat: list = int_m

    def build_random_graph(
        self, proteins: list, distributionLaw=lambda x: True, firstCase=None
    ) -> None:

        proteins_computed = set()
        int_list: list = []
        int_dict: dict = {}

        for i, prot1 in enumerate(proteins):
            for j, prot2 in enumerate(proteins):
                # case 1 : prot1 == prot2 and both are first letter
                if i == 0 and j == 0:
                    continue

                # case 2 : prot1 != prot2 and both are not first letter (i.e eq : i==0 and j==1)
                elif firstCase is not None and i == 0 and j == 1:
                    firstCase(prot1, prot2, int_list, int_dict)

                # case 3 : prot1 != prot2 for every other index
                elif prot2 not in proteins_computed and prot1 != prot2:
                    # we test against the distribution law, who evantulay draw a random againts a distribution law
                    if distributionLaw(prot1, int_dict, int_list):
                        int_list.append([prot1, prot2])
                        self.add_edge_to_dict(int_dict, prot1, prot2)
            proteins_computed.add(prot1)

        #  catch case of empty graphs
        if len(int_list) < 1:
            raise ValueError("No edges in the graph")

        # build the matrice and re-refference all needed class object
        int_m: list = [
            [1 if y in int_dict[x] else 0 for y in sorted(int_dict)]
            for x in sorted(int_dict)
        ]

        self.int_list: list = int_list
        self.int_dict: dict = int_dict
        self.int_mat: list = int_m
        self.proteins: list = proteins

    def build_ER_random_graph(self, p: float, proteins: list):
        """
        This methods use Erdös-Rényi model to generate a random graph

        Args:
            p (float): probability of an edge to be created
            proteins (list): list of proteins to add to the graph

        Returns:
            : a random graph
        """
        # the set store the prots that have already been computed
        proteins = self.proteins + proteins

        # args/kwags for more complexe rules without crashes - CHECK if more complexe rules are added later, not consistent
        unfiromRandom = lambda *args, **kwargs: random.random() < p
        self.build_random_graph(proteins, unfiromRandom)

    def build_BA_random_graph(self, proteins: list):
        """
        This method use Barabasi-Albert modezl to generate a random graph
        """

        # while wiating for teatcher anwser we asume that m0 nodes are all prots already in the graph
        # we will add eges
        proteins = self.proteins + proteins

        # we need a special case for the first edges to be created (i.e m0 nodes) otherwise we will divede by 0
        def firstCase(prot1, prot2, int_list, int_dict):
            self.add_edge_to_dict(int_dict, prot1, prot2)
            int_list.append([prot1, prot2])

        # distribution law of barabasi albert model, random VS degre of prot to link/ total links
        degree_distribution_law = (
            lambda prot, int_dict, int_list: random.random()
            < self.get_degree(prot, int_dict) / len(int_list)
        )

        # create the new random graph
        self.build_random_graph(proteins, degree_distribution_law, firstCase)

    def __build_proteins_list(self) -> None:
        """Build a list of the prots of the graph
        Used cause keys() method do not return a list type and this code is used multiple times

        Returns:
            (list): list of the prots
        """
        self.proteins = [prot for prot in self.int_dict.keys()]

    # -- getter/setter--

    @property
    def int_list(self) -> list:  # type: ignore
        return self.__int_list

    @int_list.setter
    def int_list(self, v: list) -> None:  # type: ignore
        self.__int_list = v

    @property
    def int_dict(self) -> dict:  # type: ignore
        return self.__int_dict

    @int_dict.setter
    def int_dict(self, v: dict) -> None:  # type: ignore
        self.__int_dict = v

    @property
    def int_mat(self) -> list:  # type: ignore
        return self.__int_mat

    @int_mat.setter
    def int_mat(self, v: list) -> None:  # type: ignore
        self.__int_mat = v

    @property
    def proteins(self) -> list:
        return self.__proteins

    @proteins.setter
    def proteins(self, v: list) -> None:
        self.__proteins = v

    # -- utils method --

    def count_vertices(self) -> int:
        """Return number of vertices in the graph

        Returns:
            (int): total of vertices
        """
        return len(self.proteins)

    def count_edges(self) -> int:
        """Return number of edges in the graph

        Returns:
            (int): number of edges
        """
        return len(self.int_list)

    def get_degree(self, prot: str, int_dict_in=None) -> int:
        """Return degree of the given prot

        Args:
        prot (str): the given prot

        Raises:
            (ValueError): raise an error if the prot is not in the graph

        Returns:
            (int): the degree of the prot
        """
        int_dict: dict = self.__int_dict if int_dict_in is None else int_dict_in
        if not prot in int_dict:
            raise ValueError("prot not in the graph")
        else:
            return len(int_dict[prot])

    def get_max_degree(self) -> list:
        """Get the prots with max degree in the graph

        Returns:
            (list): prot.s with the max degree.s
        """
        degrees: dict = {}
        for prot in self.__int_dict:
            if not self.get_degree(prot) in degrees:
                degrees[self.get_degree(prot)] = [prot]
            else:
                degrees[self.get_degree(prot)].append(prot)

        return degrees[max([key for key in degrees.keys()])]

    def get_ave_degree(self) -> float:
        """Get the average degree of the prots of the graph

        Returns:
            (float): average degree of the graph
        """
        return mean([len(self.__int_dict[prot]) for prot in self.__int_dict])

    def count_degree(self, deg: int) -> int:
        """Return number of protein with the given degree in the graph

        Args:
            deg (int): degree

        Returns:
            (int): number of prot with the given degree
        """
        return len([1 for prot in self.__int_dict if len(self.__int_dict[prot]) == deg])

    def histogram_degree(self, dmin: int, dmax: int) -> None:
        """Print the number of prot with degree between dmin and dmax

        Args:
            dmin (int): minimum degree
            dmax (int): maximum degree
        """
        for degree in range(dmin, dmax + 1):
            print(str(degree) + " : " + "*" * self.count_degree(degree))

    def density(self) -> float:
        """Return the density of th graph

        Returns:
            (float): The density of the graph
        """
        E = self.count_edges()
        A = len(self.proteins)
        return E / (A * (A - 1) / 2)

    def count_distinct_neigh_pairs(self, prot: str) -> list:
        """Return the pairs of the distincts neighboors of the prot

        Args:
            prot (str): the given prot

        Returns:
            (list): pairs of distinct neighboors
        """
        neighbours: list = self.int_dict[prot]
        duos = []
        for i in neighbours:
            for j in neighbours:
                duo = [i, j]
                duo.sort()
                if (not duo in duos) and (i != j):
                    duos.append(duo)

        return duos

    def clustering(self, prot: str) -> float:
        """Return the clustering coeficient of the given prot

        Args:
            prot (str): the given prot

        Returns:
            (float): The clustering coef of the given prot
        """
        distinct = self.count_distinct_neigh_pairs(prot)
        triangles = sum(
            [
                1
                for duo in distinct
                if (duo in self.int_list) or ([duo[1], duo[0]] in self.int_list)
            ]
        )

        return triangles / len(distinct)

    def get_dict_CC(self) -> dict:
        """Return a dict of all connex component in the graph with number as key and
           list of vertices in the cc as values

        Returns:
            dict: connex component dict
        """
        int_list: list = deepcopy(self.int_list)
        cc_dict: dict = {}
        cc_number: int = 1
        proteins = deepcopy(self.proteins)
        added_proteins: list = []

        for protein in proteins:
            if not protein in added_proteins:
                verts_in_cc_list, int_list = self.get_spec_CC(
                    protein=protein, int_list=int_list
                )
                cc_dict[str(cc_number)] = verts_in_cc_list
                cc_number += 1
                added_proteins.extend(verts_in_cc_list)

        return cc_dict

    def get_spec_CC(self, protein: str, int_list: list = []) -> Tuple[list]:
        """take a protein and a part (or full) of interactome and return the
           connex component for this prot and an update int_list

        Args:
            protein (str): protein which we lookg for the connex component
            int_list (list, optional): partial or full by default int_list

        Returns:
            Tuple[list]: connex component and the update int_list
        """
        int_list = deepcopy(self.int_list) if not int_list else int_list
        end_of_component: bool = False  # check if we gone through all the cc
        cc_vertices: list = []  # store protein that compose of the cc
        # the protein that we use to search for other component in the graph
        searched_prot: str = protein
        edge_in_cc: list = []  # keep edge that are part of the cc
        # prot that we already use to search for other component in the graph
        computed_prots: list = []

        while not end_of_component:

            # find edges that have the searched_prot as vertice
            end_of_component = True
            for edge in int_list:
                if searched_prot in edge:
                    edge_in_cc.append(edge)
                    end_of_component = False

            # add all vertices in the edge found that are not already store in the list
            for edge in edge_in_cc:
                for prot in edge:
                    if not prot in cc_vertices:
                        cc_vertices.append(prot)

            # select a new (non allready used) prot to search for edge in the graph
            computed_prots.append(searched_prot)
            for prot in cc_vertices:
                if not prot in computed_prots:
                    searched_prot = prot

            # remove found edge in the interactome list
            for edge in edge_in_cc:
                if edge in int_list:
                    int_list.remove(edge)

        return (cc_vertices, int_list)

    def get_cc_representation(self, cc_dict: dict) -> list:
        """get the list cc composition with the length as str

        Args:
            cc_dict (dict): dictionary of all the cc

        Returns:
            list: list of cc representation
        """
        representation_list: list = []
        for key in sorted(cc_dict, key=lambda k: len(cc_dict[k]), reverse=True):
            representation_list.append(
                str(len(cc_dict[key])) + " " + " ".join(cc_dict[key])
            )
        return representation_list

    def count_CC(self) -> None:
        """Print all the cc with length and composition"""

        for line in self.get_cc_representation(cc_dict=self.get_dict_CC()):
            print(line)

    def write_CC(self) -> None:
        """write all cc representation in an outfile"""

        with open("connex_component.txt", "w") as wipe:
            wipe.write("")
        with open("connex_component.txt", "a") as out:
            for line in self.get_cc_representation(cc_dict=self.get_dict_CC()):
                out.write(line + "\n")

    def extract_CC(self, prot: str) -> list:
        """return the list of vertices in the cc of the given protein

        Args:
            prot (str): a given protein

        Returns:
            list: list of vertices in the cc
        """
        return self.get_spec_CC(protein=prot)[0]

    def compute_CC(self):
        """return the list of the number of the corresponding cc for each prot in the
           protein list (same index)

        Returns:
            list: list of number of cc (same index as self.proteins)
        """
        lcc: list = []
        cc_dict: dict = self.get_dict_CC()
        for prot in self.proteins:
            for key in cc_dict:
                if prot in cc_dict[key]:
                    lcc.append(key)

        return lcc


if __name__ == "__main__":
    pass
