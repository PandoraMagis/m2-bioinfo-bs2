import numpy as np
from pathlib import Path
from typing import Tuple, Generator

from utils import cd_to_parent_folder


def is_interaction_file(filename: Path) -> Tuple[bool, BaseException]:
    """Make sure the tabulate file is in a correct format

    Args:
        filename (Path): tabulate interaction file path

    Returns:
        (bool): is the file in correct format ? yes/no
        error_msg (BaseException): potential error message
    """

    interaction_reader: Generator = read_interaction_file_generator(filename=filename)
    is_first_line: bool = True
    line_count: int = 0
    line_total: int = 0
    file_is_empty: bool = True

    for line in interaction_reader:

        file_is_empty = False
        if is_first_line:
            is_first_line = False
            try:
                line_total = int(line)
            except ValueError:
                return False, ValueError("First line not an valid integer")
        else:
            line_count += 1
            link: list = line.split(" ")
            try:
                if len(line) < 3 or len(link) != 2:
                    return False, ValueError(f"Error on format in line : {line}")

            except IndexError:
                return False, ValueError(f"Error on format in line : {line}")

    if file_is_empty:
        return False, ValueError("Error: empty file")

    elif line_count != line_total:
        return False, ValueError(
            f"Wrong line number, {line_total} indicated for {line_count} line in total"
        )

    return True, None  # type: ignore


def read_interaction_file_generator(filename: Path):
    """Open the tabulate file and yield each line

    Args:
        filename (Path): tabulate interaction file path

    Yields:
        line (str): one line of the file
    """
    cd_to_parent_folder()
    with open(filename) as f:
        for line in f:
            yield line.rstrip()


# 1.2.1 Question structre 1
def read_interaction_file_dict(filename: Path) -> dict:
    """Read tabulate interaction file and return the giving interaction Dict

    Args:
        filename (Path): tabulate interaction file path

    Returns:
        dict: interaction graph as dictionary
    """
    file_check, error_msg = is_interaction_file(filename)
    if file_check:
        interaction_reader: Generator = read_interaction_file_generator(filename)
        interaction_dict: dict = {}

        # With x, y as the two entity of the relation
        for X, Y in [line.split() for line in interaction_reader if len(line) >= 3]:
            for x, y in [(X, Y), (Y, X)]:  # do the if,elif,else for x AND y
                if x not in interaction_dict:
                    interaction_dict[x] = y
                elif isinstance(interaction_dict[x], str):
                    interaction_dict[x] = [interaction_dict[x], y]
                else:
                    interaction_dict[x].append(y)

        return interaction_dict
    else:
        raise error_msg


# 1.2.2 Question structure 2
def read_interaction_file_list(filename: Path) -> list:
    """Read tabulate interaction file and return the giving list of interaction

    Args:
        filename (Path): tabulate interaction file path

    Returns:
        (list): interaction graph as list of interaction
    """
    interaction_reader: Generator = read_interaction_file_generator(filename)

    return [line.split() for line in interaction_reader if len(line) >= 3]


# 1.2.3 Question structure 3
def read_interaction_file_mat(filename: Path) -> np.matrix:
    """Read tabulate interaction file and return the giving adjacency matrix

    Args:
        filename (Path): tabulate interaction file path

    Returns:
        np.matrix: adjacency matrix of the interaction graph
    """
    int_d: dict = read_interaction_file_dict(filename)
    return np.matrix(
        [[1 if y in int_d[x] else 0 for y in sorted(int_d)] for x in sorted(int_d)]
    ), sorted(  # using sorted() on dict to make sure summit are well sorted
        int_d
    )  # type: ignore


# 1.2.4 Question structure 4
def read_interaction_file(filename: Path) -> Tuple[dict, list, np.matrix]:
    """Read tabulate interaction file and return the interaction graph in multiple ways

    Args:
        filename (Path): tabulate interaction file path

    Returns:
        Tuple[dict, list, np.matrix]: graph dictionary, interaction list, matrix and summits
    """
    file_check, error_msg = is_interaction_file(filename)
    if file_check:
        d_int: dict = read_interaction_file_dict(filename)
        l_int: list = read_interaction_file_list(filename)
        m_int: np.matrix
        l_som: list
        m_int, l_som = read_interaction_file_mat(filename)

        return d_int, l_int, m_int, l_som  # type: ignore

    else:
        raise error_msg


if __name__ == "__main__":
    pass
