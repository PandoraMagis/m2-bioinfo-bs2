import unittest
from utils import set_import_root, cd_to_parent_folder


class TestTestCase(unittest.TestCase):
    """TestCase to test if the test works"""

    def test_test(self) -> None:
        """test if the tests works"""
        self.assertTrue(True)


class TestCountVertices(unittest.TestCase):
    """TestCase for c2.count_vertices function"""

    def test_return_correct_int(self):
        count = c2.count_vertices("tests/test_files/simple_test_file.txt")
        self.assertEqual(count, 4)


class TestCleanInteractome(unittest.TestCase):
    """TestCase for c2.clean_interactome function"""

    def test_clean_interactom(self):
        c2.clean_interactome(
            filein="tests/test_files/test_clean_in.txt",
            fileout="tests/test_files/test_clean_out.txt",
        )
        with open("tests/test_files/test_clean_out.txt") as test_file:
            test_text = test_file.read()

        with open("tests/test_files/simple_test_file.txt") as test_file:
            verif_txt = test_file.read()

        self.assertEqual(verif_txt, test_text)


class TestGetDegree(unittest.TestCase):
    """TestCase for c2.get_degree function"""

    def test_return_correct_int(self):
        degree = c2.get_degree(file="tests/test_files/simple_test_file.txt", prot="A")
        self.assertEqual(degree, 3)


class TestGetMaxDegree(unittest.TestCase):
    """TestCase for c2.get_max_degree function"""

    def test_return_correct_list(self):
        degree = c2.get_max_degree(file="tests/test_files/simple_test_file.txt")
        self.assertEqual(degree, ["A", "C"])


class TestGetAveDegree(unittest.TestCase):
    """TestCase for c2.get_ave_degree function"""

    def test_return_correct_float(self):
        degree = c2.get_ave_degree(file="tests/test_files/simple_test_file.txt")
        self.assertAlmostEqual(degree, 2.5)


class TestCountDegree(unittest.TestCase):
    """TestCase for c2.count_degree function"""

    def test_return_correct_int(self):
        number = c2.count_degree(file="tests/test_files/simple_test_file.txt", deg=3)
        self.assertEqual(2, number)


if __name__ == "__main__":
    print("----\n\nLaunch test for chap2\n\n----")
    set_import_root()  # set root in order to import functions/classes
    import bs2_chap2 as c2  # import function and classes to tests

    cd_to_parent_folder()  # cd to root in order to get test file
    unittest.main()  # perform tests
