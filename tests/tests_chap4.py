import unittest
from utils import set_import_root, cd_to_parent_folder


class TestTestCase(unittest.TestCase):
    """TestCase to test if the test works"""

    def test_test(self) -> None:
        """test if the tests works"""
        self.assertTrue(True)


class TestBARandomGraph(unittest.TestCase):
    """test if BA graph generation method work"""

    # test the add of the new nodes
    def test_BA_add_proteins(self):
        int = c4.Interactome("tests/test_files/simple_test_file.txt")
        new_prot = ["E", "F", "G"]

        int.build_BA_random_graph(proteins=new_prot)
        proteins = int.proteins
        proteins.sort()
        self.assertEqual(proteins, ["A", "B", "C", "D", "E", "F", "G"])

    #   test that the object changed
    def test_BA_random_object(self):
        int = c4.Interactome("tests/test_files/simple_test_file.txt")
        base_int_dict = int.int_dict.copy()

        int.build_BA_random_graph(proteins=[])
        new_int_dict = int.int_dict.copy()

        self.assertNotEqual(base_int_dict, new_int_dict)

    # test the random exponential distribution
    def test_BA_exponential_repartition(self):
        int = c4.Interactome("tests/test_files/simple_test_file.txt")

        int.build_BA_random_graph(proteins=[f"prot{i}" for i in range(100)])

        lesser_than_five = sum([int.count_degree(i) for i in range(1, 6)])
        top_five_degree = sum(
            [
                int.count_degree(i)
                for i in range(
                    int.get_degree(int.get_max_degree()[0]),
                    int.get_degree(int.get_max_degree()[0]) - 5,
                    -1,
                )
            ]
        )
        self.assertLess(top_five_degree, lesser_than_five)

    # test if the BA graph connect the first two edges (neccesary for the execution)
    def test_BA_first_edges_linked(self):
        int = c4.Interactome("tests/test_files/simple_test_file.txt")

        int.build_BA_random_graph(proteins=[])
        self.assertTrue("B" in int.int_dict["A"])


class TestERRandomGraph(unittest.TestCase):
    """test if ER graph generation method work"""

    # test that all proteins passed by user are added to the interactome
    def test_ER_add_proteins(self):
        int = c4.Interactome("tests/test_files/simple_test_file.txt")
        new_prot = ["E", "F", "G"]

        int.build_ER_random_graph(proteins=new_prot, p=0.5)
        proteins = int.proteins
        proteins.sort()
        self.assertEqual(proteins, ["A", "B", "C", "D", "E", "F", "G"])

    # test tha the object DOES change with the method
    def test_ER_random_object(self):
        int = c4.Interactome("tests/test_files/simple_test_file.txt")
        base_int_dict = int.int_dict.copy()

        int.build_ER_random_graph(p=0.5, proteins=[])
        new_int_dict = int.int_dict.copy()

        self.assertNotEqual(base_int_dict, new_int_dict)

    # test that the interactome can't be a empty graph
    def test_ER_no_links(self):
        int = c4.Interactome("tests/test_files/simple_test_file.txt")
        new_prot = ["E", "F", "G"]
        try:
            int.build_ER_random_graph(proteins=new_prot, p=0)
            err = None
        except ValueError as e:
            err = e
        self.assertIsInstance(err, ValueError)

    # test that the interactome can be a full connected graph
    def test_ER_all_edges_linked(self):
        int = c4.Interactome("tests/test_files/simple_test_file.txt")
        new_prot = ["E", "F", "G"]
        int.build_ER_random_graph(proteins=new_prot, p=1)

        self.assertEqual(int.density(), 1.0)

    # test that the repartition betwen edges correspond to the given p value
    def test_ER_random_repartition(self):
        int = c4.Interactome("tests/test_files/simple_test_file.txt")
        new_prot = [f"prot{i}" for i in range(1000)]
        int.build_ER_random_graph(proteins=new_prot, p=0.5)

        self.assertTrue(0.4 < int.density() < 0.6)


if __name__ == "__main__":
    print("----\n\nLaunch test for chap4\n\n----")
    set_import_root()  # set root in order to import functions/classes
    import bs2_chap3_4_5 as c4  # import function and classes to tests

    cd_to_parent_folder()  # cd to root in order to get test file
    unittest.main()  # perform tests

    # test :

    # test first edges got many representation (>70%)
