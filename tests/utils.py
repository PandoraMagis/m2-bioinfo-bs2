from pathlib import Path
import sys
import os


def set_import_root() -> None:
    """Set the root for testing to acces functions and class"""
    path_root = Path(__file__).parents[1] / "bs2_chapters"
    sys.path.append(str(path_root))


def cd_to_parent_folder() -> None:
    path_root = Path(__file__).parents[1]
    os.chdir(path_root)
