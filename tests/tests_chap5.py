import unittest
from utils import set_import_root, cd_to_parent_folder


class TestTestCase(unittest.TestCase):
    """TestCase to test if the test works"""

    def test_test(self) -> None:
        """test if the tests works"""
        self.assertTrue(True)


class TestCCRepresentation(unittest.TestCase):
    """TestCase to test cc_representation method"""

    def test_get_cc_representation(self):
        int = c5.Interactome(filename="tests/test_files/cc_test_file.txt")

        self.assertEqual(
            int.get_cc_representation(int.get_dict_CC()), ["4 A C B D", "3 E G F"]
        )


class TestComputeCC(unittest.TestCase):
    """TestCase to test computeCC method"""

    def test_compute_CC(self):
        int = c5.Interactome(filename="tests/test_files/cc_test_file.txt")

        self.assertEqual(int.compute_CC(), ["1", "1", "1", "1", "2", "2", "2"])


if __name__ == "__main__":
    print("----\n\nLaunch test for chap5\n\n----")
    set_import_root()  # set root in order to import functions/classes
    import bs2_chap3_4_5 as c5  # import function and classes to tests

    cd_to_parent_folder()  # cd to root in order to get test file
    unittest.main()  # perform tests
