
@echo off

@REM tests=`ls *tests*`

@REM for test in $tests
@REM do
@REM     python $test
@REM done

@REM start cd tests
call cd tests

for %%f in (*test*) do (
    echo starting : %%~f
    call python %%~f
    @REM process_in "%%~nf.in"
    @REM process_out "%%~nf.out"
)