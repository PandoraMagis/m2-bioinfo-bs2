import unittest

from utils import set_import_root, cd_to_parent_folder


class TestTestCase(unittest.TestCase):
    """TestCase to test if the test works"""

    def test_test(self) -> None:
        """test if the tests works"""
        self.assertTrue(True)


class TestIsInteractionFile(unittest.TestCase):
    """TestCase for c1.is_interaction_file function"""

    def test_return_a_tuple(self):
        """test if it return a Tuple"""
        expected_tuple = c1.is_interaction_file("tests/test_files/simple_test_file.txt")

        self.assertIsInstance(expected_tuple, tuple)

    def test_file_wrong_firstline(self):
        """test if return false,ValueError with wrong firstline"""

        file_check, error_msg = c1.is_interaction_file(
            "tests/test_files/wrong_firstline.txt"
        )

        self.assertEqual(file_check, False)
        self.assertIsInstance(error_msg, ValueError)
        self.assertEqual(error_msg.args[0], "First line not an valid integer")

    def test_file_wrong_line_count(self):
        """test if return false,ValueError with wrong line count"""

        file_check, error_msg = c1.is_interaction_file(
            "tests/test_files/wrong_line_count.txt"
        )

        self.assertEqual(file_check, False)
        self.assertIsInstance(error_msg, ValueError)
        self.assertEqual(
            error_msg.args[0],
            "Wrong line number, 4 indicated for 5 line in total",
        )

    def test_file_wrong_line_format(self):
        """test if return false,ValueError with wrong line format"""

        file_check, error_msg = c1.is_interaction_file(
            "tests/test_files/wrong_line_format.txt"
        )

        self.assertEqual(file_check, False)
        self.assertIsInstance(error_msg, ValueError)
        self.assertEqual(error_msg.args[0], "Error on format in line : A,B")

    def test_file_empty_file(self):
        """test if return false,ValueError with empty file"""

        file_check, error_msg = c1.is_interaction_file(
            "tests/test_files/empty_file.txt"
        )

        self.assertEqual(file_check, False)
        self.assertIsInstance(error_msg, ValueError)
        self.assertEqual(error_msg.args[0], "Error: empty file")


class TestReadInteractionFileGenerator(unittest.TestCase):
    """TestCase for c1.read_interaction_file_generator function"""

    def test_yield_str(self):
        generator = c1.read_interaction_file_generator(
            "tests/test_files/simple_test_file.txt"
        )

        self.assertIsInstance(next(generator), str)

    def test_yield_correct_line(self):
        generator = c1.read_interaction_file_generator(
            "tests/test_files/simple_test_file.txt"
        )
        next(generator)  # go to second line
        self.assertEqual(next(generator), "A B")


class TestReadInteractionFileDict(unittest.TestCase):
    """TestCase for c1.read_interaction_file_dict function"""

    def test_return_dict(self):
        graph = c1.read_interaction_file_dict("tests/test_files/simple_test_file.txt")
        self.assertIsInstance(graph, dict)

    def test_return_correct_dict(self):
        graph = c1.read_interaction_file_dict("tests/test_files/simple_test_file.txt")

        self.assertDictEqual(
            graph,
            {
                "A": ["B", "C", "D"],
                "B": ["A", "C"],
                "C": ["B", "A", "D"],
                "D": ["A", "C"],
            },
        )


class TestReadInteractionFileList(unittest.TestCase):
    """TestCase for c1.read_interaction_file_list function"""

    def test_return_list(self):
        graph = c1.read_interaction_file_list("tests/test_files/simple_test_file.txt")
        self.assertIsInstance(graph, list)

    def test_return_correct_list(self):
        graph = c1.read_interaction_file_list("tests/test_files/simple_test_file.txt")

        self.assertEqual(
            graph, [["A", "B"], ["B", "C"], ["C", "A"], ["D", "A"], ["C", "D"]]
        )


class TestReadInteractionFileMat(unittest.TestCase):
    """TestCase for c1.read_interaction_file_mat function"""

    def test_return_mat(self):
        pass

    def test_return_correct_mat(self):
        pass


if __name__ == "__main__":
    print("----\n\nLaunch test for chap1\n\n----")
    set_import_root()  # set root in order to import functions/classes
    import bs2_chap1 as c1  # import function and classes to tests

    cd_to_parent_folder()  # cd to root in order to get test file
    unittest.main()  # perform tests
