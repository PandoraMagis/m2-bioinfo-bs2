import unittest
from utils import set_import_root, cd_to_parent_folder


class TestTestCase(unittest.TestCase):
    """TestCase to test if the test works"""

    def test_test(self) -> None:
        """test if the tests works"""
        self.assertTrue(True)


class TestSetUpWithFile(unittest.TestCase):
    """TestCase to test if the test works"""

    def test_int_dict_is_correct(self):
        int = c3.Interactome(filename="tests/test_files/simple_test_file.txt")
        self.assertEqual(
            int.int_dict,
            {
                "A": ["B", "C", "D"],
                "B": ["A", "C"],
                "C": ["B", "A", "D"],
                "D": ["A", "C"],
            },
        )
        self.assertEqual(int.proteins, ["A", "B", "C", "D"])


class TestSetUpWithDict(unittest.TestCase):
    """TestCase to test if the test works"""

    def test_int_list_is_correct(self):
        int = c3.Interactome(
            int_dict_input={
                "A": ["B", "C", "D"],
                "B": ["A", "C"],
                "C": ["A", "D", "B"],
                "D": ["A", "C"],
            }
        )
        self.assertEqual(
            int.int_list, [["A", "B"], ["A", "C"], ["A", "D"], ["B", "C"], ["C", "D"]]
        )
        self.assertListEqual(int.proteins, ["A", "B", "C", "D"])


class TestDegreeVerticesEdges(unittest.TestCase):
    """TestCase to test if the test works"""

    def test_count_vertices(self):
        int = c3.Interactome(filename="tests/test_files/simple_test_file.txt")

        self.assertEqual(int.count_vertices(), 4)

    def test_count_edges(self):
        int = c3.Interactome(filename="tests/test_files/simple_test_file.txt")

        self.assertEqual(int.count_edges(), 5)

    def test_get_degree(self):
        int = c3.Interactome(filename="tests/test_files/simple_test_file.txt")

        self.assertEqual(int.get_degree("A"), 3)

    def test_get_max_degree(self):
        int = c3.Interactome(filename="tests/test_files/simple_test_file.txt")

        self.assertListEqual(int.get_max_degree(), ["A", "C"])

    def test_get_avec_degree(self):
        int = c3.Interactome(filename="tests/test_files/simple_test_file.txt")

        self.assertAlmostEqual(2.5, int.get_ave_degree())

    def count_degree(self):
        int = c3.Interactome(filename="tests/test_files/simple_test_file.txt")

        self.assertEqual(int.count_degree(3), 2)


class TestDensity(unittest.TestCase):
    """TestCase to test if the test works"""

    def test_density(self):
        int = c3.Interactome(filename="tests/test_files/simple_test_file.txt")

        self.assertAlmostEqual(0.833333333, int.density())


class TestClustering(unittest.TestCase):
    """TestCase to test if the test works"""

    def test_clustering(self):
        int = c3.Interactome(filename="tests/test_files/simple_test_file.txt")

        self.assertAlmostEqual(0.6666666666666666, int.clustering("A"))


if __name__ == "__main__":
    print("----\n\nLaunch test for chap3\n\n----")
    set_import_root()  # set root in order to import functions/classes
    import bs2_chap3_4_5 as c3  # import function and classes to tests

    cd_to_parent_folder()  # cd to root in order to get test file
    unittest.main()  # perform tests
